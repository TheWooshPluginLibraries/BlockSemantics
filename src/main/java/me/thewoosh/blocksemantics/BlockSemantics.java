package me.thewoosh.blocksemantics;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.type.Slab;
import org.bukkit.block.data.type.Stairs;
import org.jetbrains.annotations.NotNull;

public class BlockSemantics {

    public static boolean isSlab(final @NotNull Material material) {
        return switch (material) {
            case SANDSTONE_SLAB, SMOOTH_QUARTZ_SLAB, ACACIA_SLAB, ANDESITE_SLAB, BIRCH_SLAB, BLACKSTONE_SLAB, BRICK_SLAB, COBBLESTONE_SLAB, CRIMSON_SLAB, CUT_RED_SANDSTONE_SLAB, CUT_SANDSTONE_SLAB, DARK_OAK_SLAB, DARK_PRISMARINE_SLAB, DIORITE_SLAB, END_STONE_BRICK_SLAB, GRANITE_SLAB, JUNGLE_SLAB, MOSSY_COBBLESTONE_SLAB, MOSSY_STONE_BRICK_SLAB, NETHER_BRICK_SLAB, OAK_SLAB, PETRIFIED_OAK_SLAB, POLISHED_ANDESITE_SLAB, POLISHED_BLACKSTONE_BRICK_SLAB, POLISHED_BLACKSTONE_SLAB, POLISHED_DIORITE_SLAB, POLISHED_GRANITE_SLAB, PRISMARINE_BRICK_SLAB, PRISMARINE_SLAB, PURPUR_SLAB, QUARTZ_SLAB, RED_NETHER_BRICK_SLAB, RED_SANDSTONE_SLAB, SMOOTH_RED_SANDSTONE_SLAB, SMOOTH_SANDSTONE_SLAB, SMOOTH_STONE_SLAB, SPRUCE_SLAB, STONE_BRICK_SLAB, STONE_SLAB, WARPED_SLAB -> true;
            default -> false;
        };
    }

    public static boolean isFullBlock(final @NotNull Block block) {
        if (block.getState().getBlockData() instanceof Slab) {
            return ((Slab) block.getState().getBlockData()).getType() == Slab.Type.DOUBLE;
        }

        return switch (block.getType()) {
            case BARRIER, BEDROCK, DEAD_BRAIN_CORAL_BLOCK, DEAD_HORN_CORAL_BLOCK, DIRT, GLOWSTONE, GRAVEL, GRASS_BLOCK, INFESTED_CRACKED_STONE_BRICKS, OAK_LOG, OAK_PLANKS, SPRUCE_LOG, STONE -> true;
            default -> false;
        };
    }

    public static boolean isRail(final @NotNull Material material) {
        return switch (material) {
            case RAIL, ACTIVATOR_RAIL, DETECTOR_RAIL, POWERED_RAIL -> true;
            default -> false;
        };
    }

    public static boolean isStairs(final @NotNull Block block) {
        return block.getState().getBlockData() instanceof Stairs;
    }

}
