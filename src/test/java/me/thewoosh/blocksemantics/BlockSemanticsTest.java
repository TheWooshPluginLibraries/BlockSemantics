package me.thewoosh.blocksemantics;

import junit.framework.TestCase;
import org.bukkit.Material;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;

public class BlockSemanticsTest extends TestCase {

    @SuppressWarnings("deprecation")
    private void testFunctionForName(final @NotNull Function<Material, Boolean> function,
                                     final @NotNull String condition) {
        for (Material material : Material.values()) {
            if (material.isLegacy()) {
                continue;
            }

            boolean nameCondition = material.name().contains(condition);
            assertEquals(
                    "Expected material '%s' to %s a '%s'"
                        .formatted(material.name(), nameCondition ? "be" : "not be", condition),
                    nameCondition,
                    (boolean) function.apply(material));
        }
    }

    public void testIsSlab() {
        testFunctionForName(BlockSemantics::isSlab, "SLAB");
    }

    public void testIsFullBlock() {
        // don't test this atm.
    }

    public void testIsRail() {
        testFunctionForName(BlockSemantics::isRail, "RAIL");
    }

    public void testIsStairs() {
        // don't test this atm.
    }

}